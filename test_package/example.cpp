#include <iostream>
#include "asio.hpp"

using asio::ip::tcp;

int main(int argc, char* argv[])
{
  asio::io_service io_service;
  tcp::socket socket(io_service);
  return 0;
}

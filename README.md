# Conan Asio Library #

[![Build status](https://ci.appveyor.com/api/projects/status/bsna9lyak2hu5hcn?svg=true)](https://ci.appveyor.com/project/genvid_fninoles/conan-asio)

[Asio](https://think-async.com) is a cross-platform C++ library for
network and low-level I/O programming that provides developers with a
consistent asynchronous model using a modern C++ approach.

This is a non-boost version of the library.

## Building ##

To build, simply do:

	#console
	
	pip install -r requirements
	py build.py

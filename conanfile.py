from conans import ConanFile
from conans.errors import ConanException

class AsioConan(ConanFile):
    name = "asio"
    version = "1.10.8"
    license = "MIT"
    url = "https://bitbucket.org/genvidtech/conan-asio"
    # No settings/options are necessary, this is header only
    description = "Async-IO C++ Header Library."

    def config_options(self):
        try:
            compiler = self.settings.compiler
        except ConanException:
            return
        else:
            if compiler == 'gcc':
                if float(compiler.version.value) >= 5.1:
                    if compiler.libcxx != 'libstdc++11':
                        raise ConanException("You must use the setting compiler.libcxx=libstdc++11")

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code in the folder and
        use exports instead of retrieving it with this source() method
        '''
        self.run("git clone https://github.com/chriskohlhoff/asio")
        self.run("cd asio && git checkout asio-1-10-8")

    def package(self):
        self.copy("*.hpp", "include", "asio/asio/include")
        self.copy("*.ipp", "include", "asio/asio/include")

import os
import sys

from conan.packager import ConanMultiPackager

config = dict(
    CONAN_REFERENCE="conan-asio/1.10.8",
    CONAN_USERNAME="genvidtech",
    CONAN_LOGIN_USERNAME="genvidtech",
    CONAN_CHANNEL="1.4.0",
)

if sys.platform == "win32":
    config.update(
        CONAN_VISUAL_VERSIONS="14",
    )
else:
    config.update(
        CONAN_GCC_VERSIONS="6.2"
    )

def setup():
    for k, v in config.items():
        os.environ.setdefault(k, v)
    
def main():
    setup()
    builder = ConanMultiPackager()
    builder.add_common_builds()
    builder.run()


if __name__ == "__main__":
    main()

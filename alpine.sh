#!/bin/sh -e
apk add --no-cache \
	cmake \
    coreutils \
    g++ \
    git \
    linux-headers \
    make \
	makedepend \
    python \
    py2-pip
pip install -r requirements.txt
conan config set settings_defaults.compiler.libcxx=libstdc++11
